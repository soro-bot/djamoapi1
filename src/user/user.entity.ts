import { ApiProperty } from "@nestjs/swagger";
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()

export class User {
    @PrimaryGeneratedColumn()
    id : number;

    @ApiProperty()
    @Column()
    title : string;

    @ApiProperty()
    @Column()
    content : string;

    @ApiProperty()
    @Column()
    date: string;
    
    @ApiProperty()
    @Column()
    status: boolean;
}