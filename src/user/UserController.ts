import { Controller } from '@nestjs/common';
import { CreateManyDto, Crud, CrudController, CrudRequest, GetManyDefaultResponse } from '@nestjsx/crud';
import { User } from './user.entity';
import { UserService } from './user.service';


@Crud({
    model: {
        type: User
    }
})
@Controller('user')
export class UserController implements CrudController<User> {
    constructor(public service: UserService) { }


    getManyBase?(req: CrudRequest): Promise<GetManyDefaultResponse<User> | User[]> {
        throw new Error('Method not implemented.');
    }
    getOneBase?(req: CrudRequest): Promise<User> {
        throw new Error('Method not implemented.');
    }
    createOneBase?(req: CrudRequest, dto: User): Promise<User> {
        throw new Error('Method not implemented.');
    }
    createManyBase?(req: CrudRequest, dto: CreateManyDto<User>): Promise<User[]> {
        throw new Error('Method not implemented.');
    }
    updateOneBase?(req: CrudRequest, dto: User): Promise<User> {
        throw new Error('Method not implemented.');
    }
    replaceOneBase?(req: CrudRequest, dto: User): Promise<User> {
        throw new Error('Method not implemented.');
    }
    deleteOneBase?(req: CrudRequest): Promise<void | User> {
        throw new Error('Method not implemented.');
    }
    recoverOneBase?(req: CrudRequest): Promise<void | User> {
        throw new Error('Method not implemented.');
    }
}
