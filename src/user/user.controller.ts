import { Controller, Get } from '@nestjs/common';
import { Crud, CrudController } from '@nestjsx/crud';
import { User } from './user.entity';

@Crud({
    model: {
        type: User
    }
})
@Controller('user')
export class UserController implements CrudController <User> {
   
}
